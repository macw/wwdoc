package com.macw.wwdoc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.macw.wwdoc.controller.BaseController;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-05-05
 */
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {

}
