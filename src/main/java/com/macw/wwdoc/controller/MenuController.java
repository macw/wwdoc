package com.macw.wwdoc.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.macw.wwdoc.common.annotation.ControllerEndpoint;
import com.macw.wwdoc.common.entity.FebsConstant;
import com.macw.wwdoc.common.entity.FebsResponse;
import com.macw.wwdoc.common.entity.MenuTree;
import com.macw.wwdoc.common.exception.FebsException;
import com.macw.wwdoc.common.utils.FebsUtil;
import com.macw.wwdoc.entity.Config;
import com.macw.wwdoc.entity.Menu;
import com.macw.wwdoc.entity.User;
import com.macw.wwdoc.entity.vo.MenuVo;
import com.macw.wwdoc.mapper.MenuMapper;
import com.macw.wwdoc.service.ICategoryService;
import com.macw.wwdoc.service.IConfigService;
import com.macw.wwdoc.service.IMenuService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-01-14
 */
@RestController
@RequestMapping("/menu")
public class MenuController extends BaseController{

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IMenuService iMenuService;

    @Resource
    private IConfigService iConfigService;

    @Resource
    private ICategoryService iCategoryService;

    @RequestMapping("/index")
    public Map index(){
        MenuVo menuVo = iMenuService.listMenuVo(1,"控制台","fa fa-home");
        Map<String,Object> map2 = new HashMap<>(16);
        map2.put("control", menuVo);
        Map map = info();
        map.put("menuInfo",map2);
        return map;
    }

    private Map info(){
        Config clearUrl = iConfigService.getOne(new QueryWrapper<Config>().lambda().eq(Config::getConfigKey, "clearUrl"));
        Map<String,Object> clearUrlMap = new HashMap<>(16);
        clearUrlMap.put("clearUrl", clearUrl.getConfigValue());

        Map<String,Object> homeInfoMap = new HashMap<>(16);
        homeInfoMap.put("title", "首页");
        homeInfoMap.put("icon", "fa fa-home");
        homeInfoMap.put("href", "/user/toWelcome");

        Config logoImage = iConfigService.getOne(new QueryWrapper<Config>().lambda().eq(Config::getConfigKey, "logoImage"));
        Map<String,Object> logoInfoMap = new HashMap<>(16);
        logoInfoMap.put("title", "wwDoc");
        logoInfoMap.put("href", "/user/toWelcome");
        logoInfoMap.put("image", logoImage.getConfigValue());

        Map<String,Object> map = new HashMap<>(16);
        map.put("clearInfo", clearUrlMap);
        map.put("homeInfo",homeInfoMap);
        map.put("logoInfo", logoInfoMap);
        return map;

    }

    @RequestMapping("/doc")
    @ControllerEndpoint(operation = "查询DOC", exceptionMessage = "查询DOC失败")
    public Map doc(){
        User user = getUser();
        Integer proId = getProId();
        MenuVo menuVo2 = iCategoryService.listMenuVo(user.getUserId(),proId,"返回项目列表","fa fa-undo");
        Map<String,Object> map2 = new HashMap<>(16);
        map2.put("docs", menuVo2);

        Map map = info();
        map.put("menuInfo",map2);
        return map;
    }

    @RequestMapping("/to404")
    public ModelAndView to404(){
        return new ModelAndView(thyme+"/docs/category/404");
    }

    @GetMapping("system/menu")
    @RequiresPermissions("menu:view")
    public ModelAndView systemMenu() {
        return new ModelAndView(thyme+"/menu/menu");
    }



    @GetMapping("{username}")
    public FebsResponse getUserMenus(@NotBlank(message = "{required}") @PathVariable String username) throws FebsException {
        User currentUser = getCurrentUser();
        if (!StringUtils.equalsIgnoreCase(username, currentUser.getUserName())) {
            throw new FebsException("您无权获取别人的菜单");
        }
        MenuTree<Menu> userMenus = this.iMenuService.findUserMenus(username);
        return new FebsResponse().data(userMenus);
    }

    @GetMapping("tree")
    @ControllerEndpoint(exceptionMessage = "获取菜单树失败")
    public FebsResponse getMenuTree(Menu menu) {
        MenuTree<Menu> menus = this.iMenuService.findMenus(menu);
        return new FebsResponse().success().data(menus.getChilds());
    }

    @PostMapping
    @RequiresPermissions("menu:add")
    @ControllerEndpoint(operation = "新增菜单/按钮", exceptionMessage = "新增菜单/按钮失败")
    public FebsResponse addMenu(@Valid Menu menu) {
        this.iMenuService.createMenu(menu);
        return new FebsResponse().success();
    }

    @GetMapping("delete/{menuIds}")
    @RequiresPermissions("menu:delete")
    @ControllerEndpoint(operation = "删除菜单/按钮", exceptionMessage = "删除菜单/按钮失败")
    public FebsResponse deleteMenus(@NotBlank(message = "{required}") @PathVariable String menuIds) {
        this.iMenuService.deleteMeuns(menuIds);
        return new FebsResponse().success();
    }

    @PostMapping("update")
    @RequiresPermissions("menu:update")
    @ControllerEndpoint(operation = "修改菜单/按钮", exceptionMessage = "修改菜单/按钮失败")
    public FebsResponse updateMenu(@Valid Menu menu) {
        this.iMenuService.updateMenu(menu);
        return new FebsResponse().success();
    }



}
