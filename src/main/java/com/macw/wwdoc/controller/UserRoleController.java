package com.macw.wwdoc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.macw.wwdoc.controller.BaseController;

/**
 * <p>
 * 用户角色关联表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-05-05
 */
@RestController
@RequestMapping("/user-role")
public class UserRoleController extends BaseController {

}
