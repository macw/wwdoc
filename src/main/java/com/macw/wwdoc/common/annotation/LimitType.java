package com.macw.wwdoc.common.annotation;

/**
 * @author MrBird
 */
public enum LimitType {
    /**
     * 传统类型
     */
    CUSTOMER,
    /**
     *  根据 IP地址限制
     */
    IP
}
