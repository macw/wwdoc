package com.macw.wwdoc.common.utils;

public class IntegerUtils {

    public static boolean isBlank(Integer integer) {
        if(integer==null || integer==0) {
            return true;
        }
        return false;
    }
    public static boolean isNotBlank(Integer integer) {
        if(integer==null || integer==0) {
            return false;
        }
        return true;
    }

    public static boolean isLongBlank(Long longs) {
        if(longs==null || longs==0) {
            return true;
        }
        return false;
    }
    public static boolean isLongNotBlank(Long longs) {
        if(longs==null || longs==0) {
            return false;
        }
        return true;
    }

}