package com.macw.wwdoc.common.entity;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 常量
 *
 * @author MrBird
 */
public class FebsConstant {



    // 排序规则：降序
    public static final String ORDER_DESC = "desc";
    // 排序规则：升序
    public static final String ORDER_ASC = "asc";


    // 允许下载的文件类型，根据需求自己添加（小写）
    public static final String[] VALID_FILE_TYPE = {"xlsx", "zip"};


    /**
     * {@link
     * //getDataTable 中 HashMap 默认的初始化容量
     */
    public static final int DATA_MAP_INITIAL_CAPACITY = 4;

    /**
     * 异步线程池名称
     */
    public static final String ASYNC_POOL = "febsAsyncThreadPool";



    public static final String  SOCKET_IP = getIpAddress();

    private static  String getIpAddress(){
        String address = "127.0.0.1";
        try {
            InetAddress localHost = InetAddress.getLocalHost();
            address = localHost.getHostAddress();
            return address;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return null;
        }
    }

    // 前端页面路径前缀
    public static final String VIEW_PREFIX = "views";


}
