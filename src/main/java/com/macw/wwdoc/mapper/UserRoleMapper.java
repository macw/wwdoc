package com.macw.wwdoc.mapper;

import com.macw.wwdoc.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关联表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-05-05
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
