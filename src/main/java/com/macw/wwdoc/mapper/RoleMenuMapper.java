package com.macw.wwdoc.mapper;

import com.macw.wwdoc.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色菜单关联表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-05-05
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}
