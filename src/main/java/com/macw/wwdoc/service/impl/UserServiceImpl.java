package com.macw.wwdoc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.macw.wwdoc.common.authentication.ShiroRealm;
import com.macw.wwdoc.common.entity.FebsConstant;
import com.macw.wwdoc.common.entity.QueryRequest;
import com.macw.wwdoc.common.utils.FebsUtil;
import com.macw.wwdoc.common.utils.SortUtil;
import com.macw.wwdoc.entity.User;
import com.macw.wwdoc.entity.UserRole;
import com.macw.wwdoc.mapper.UserMapper;
import com.macw.wwdoc.service.IUserRoleService;
import com.macw.wwdoc.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-01-14
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private IUserRoleService userRoleService;
    @Autowired
    private ShiroRealm shiroRealm;

    @Override
    public User findByName(String username) {
        return this.baseMapper.findByName(username);
    }


    @Override
    public User getCurrentUser() {
        return (User) SecurityUtils.getSubject().getPrincipal();
    }


}
