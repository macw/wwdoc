package com.macw.wwdoc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.macw.wwdoc.common.authentication.ShiroRealm;
import com.macw.wwdoc.common.entity.MenuTree;
import com.macw.wwdoc.common.utils.TreeUtil;
import com.macw.wwdoc.entity.Menu;
import com.macw.wwdoc.entity.vo.MenuVo;
import com.macw.wwdoc.mapper.MenuMapper;
import com.macw.wwdoc.service.IMenuService;
import com.macw.wwdoc.service.IRoleMenuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-01-14
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Resource
    private MenuMapper menuMapper;

    @Resource
    private ShiroRealm shiroRealm;

    @Resource
    private IRoleMenuService roleMenuService;

    @Override
    public MenuVo listMenuVo(Integer identify,String title,String icon) {
        List<MenuVo> menuVos = menuMapper.listMenuVo(identify);
        for (MenuVo menuVo : menuVos) {
            if (menuVo.getChild()!=null) {
                for (MenuVo menuVoChild :menuVo.getChild()) {
                    menuVoChild.setTarget("_self");
                }
            }
            if (menuVo.getChild().size()==0){
                menuVo.setChild(null);
            }
            menuVo.setTarget("_self");
        }
        MenuVo menuVo = new MenuVo();
        menuVo.setTitle(title);
        menuVo.setIcon(icon);
        menuVo.setTarget("_self");
        menuVo.setChild(menuVos);

        return menuVo;
    }

    @Override
    public List<Menu> findUserPermissions(String username) {
        return this.baseMapper.findUserPermissions(username);
    }


    @Override
    public MenuTree<Menu> findUserMenus(String username) {
        //获取
        List<Menu> menus = this.baseMapper.findUserMenus(username);
        List<MenuTree<Menu>> trees = this.convertMenus(menus);
        MenuTree<Menu> menuTree = TreeUtil.buildMenuTree(trees);
        System.out.println(menuTree);
        return menuTree;
    }

    @Override
    public MenuTree<Menu> findMenus(Menu menu) {
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(menu.getTitle())) {
            queryWrapper.lambda().like(Menu::getTitle, menu.getTitle());
        }
        queryWrapper.lambda().orderByAsc(Menu::getSort);
        List<Menu> menus = this.baseMapper.selectList(queryWrapper);
        List<MenuTree<Menu>> trees = this.convertMenus(menus);
        return TreeUtil.buildMenuTree(trees);
    }

    @Override
    public List<Menu> findMenuList(Menu menu) {
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(menu.getTitle())) {
            queryWrapper.lambda().like(Menu::getTitle, menu.getTitle());
        }
        queryWrapper.lambda().orderByAsc(Menu::getMenuId).orderByAsc(Menu::getSort);
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional
    public void createMenu(Menu menu) {
        this.baseMapper.insert(menu);
    }


    @Override
    @Transactional
    public void updateMenu(Menu menu) {
        this.baseMapper.updateById(menu);

        shiroRealm.clearCache();
    }

    @Override
    @Transactional
    public void deleteMeuns(String menuIds) {
        String[] menuIdsArray = menuIds.split(StringPool.COMMA);
        this.delete(Arrays.asList(menuIdsArray));
        shiroRealm.clearCache();
    }

    private List<MenuTree<Menu>> convertMenus(List<Menu> menus) {
        List<MenuTree<Menu>> trees = new ArrayList<>();
        menus.forEach(menu -> {
            MenuTree<Menu> tree = new MenuTree<>();
            tree.setId(String.valueOf(menu.getMenuId()));
            tree.setParentId(String.valueOf(menu.getPid()));
            tree.setTitle(menu.getTitle());
            tree.setIcon(menu.getIcon());
            tree.setHref(menu.getPath());
            tree.setData(menu);
            trees.add(tree);
        });
        return trees;
    }


    private void delete(List<String> menuIds) {
        List<String> list = new ArrayList<>(menuIds);
        removeByIds(menuIds);

        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Menu::getPid, menuIds);
        List<Menu> menus = baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(menus)) {
            List<String> menuIdList = new ArrayList<>();
            menus.forEach(m -> menuIdList.add(String.valueOf(m.getMenuId())));
            list.addAll(menuIdList);
            this.roleMenuService.deleteRoleMenusByMenuId(list);
            this.delete(menuIdList);
        } else {
            this.roleMenuService.deleteRoleMenusByMenuId(list);
        }
    }


}
