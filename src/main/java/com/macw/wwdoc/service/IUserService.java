package com.macw.wwdoc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.macw.wwdoc.common.entity.QueryRequest;
import com.macw.wwdoc.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-01-14
 */
public interface IUserService extends IService<User> {

    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return 用户
     */
    User findByName(String username);




    /**
     * 获取当前登录用户
     * @return
     */
    User getCurrentUser();

}
