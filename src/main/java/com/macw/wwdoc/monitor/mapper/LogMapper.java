package com.macw.wwdoc.monitor.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.macw.wwdoc.monitor.entity.SystemLog;

/**
 * @author MrBird
 */
public interface LogMapper extends BaseMapper<SystemLog> {

}
