package com.macw.wwdoc.monitor.controller;

import com.macw.wwdoc.common.entity.FebsResponse;
import com.macw.wwdoc.controller.BaseController;
import com.macw.wwdoc.monitor.entity.ActiveUser;
import com.macw.wwdoc.monitor.service.ISessionService;
import com.macw.wwdoc.service.IUserRoleService;
import com.macw.wwdoc.service.IUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author MrBird
 */
@RestController
@RequestMapping("session")
public class SessionController extends BaseController {

    @Autowired
    private ISessionService sessionService;
    @Autowired
    private IUserService userService;
    @Resource
    private IUserRoleService userRoleService;

    @GetMapping("list")
    @RequiresPermissions("online:view")
    public FebsResponse list(String username) {
        List<ActiveUser> list = sessionService.list(username);
        Map<String, Object> data = new HashMap<>(16);
        data.put("rows", list);
        data.put("total", CollectionUtils.size(list));
        return new FebsResponse().success().data(data);
    }

    @GetMapping("delete/{id}")
    @RequiresPermissions("user:kickout")
    public FebsResponse forceLogout(@PathVariable String id) {
        sessionService.forceLogout(id);
        return new FebsResponse().success();
    }
}
/***
 *  list.forEach(i ->{
 *             User u = userService.getById(i.getUserId());
 *             if(u.getDeptId() != null && !u.getUsername().equals("admin")){
 *                 Dept dept = deptService.getById(u.getDeptId());
 *                 i.setDeptName(dept.getDeptName());
 *             }
 *         });
 *
 *         list.contains(new ActiveUser());
 *
 *         User currentUser = getCurrentUser();
 *
 *         UserRole userRole = userRoleService.getOne(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUserId, currentUser.getUserId(), false));
 *         if (userRole.getRoleId() == Role.ROLE_TEACHER){
 *             List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, currentUser.getUserId()));
 *             userDeptList.
 *
 *         }
 */
