package com.macw.wwdoc.monitor.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.macw.wwdoc.Constant;
import com.macw.wwdoc.common.annotation.ControllerEndpoint;
import com.macw.wwdoc.common.entity.FebsResponse;
import com.macw.wwdoc.common.entity.QueryRequest;
import com.macw.wwdoc.controller.BaseController;
import com.macw.wwdoc.monitor.entity.SystemLog;
import com.macw.wwdoc.monitor.service.ILogService;
import com.macw.wwdoc.util.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

/**
 * @author MrBird
 */
@Slf4j
@RestController
@RequestMapping("log")
public class LogController extends BaseController {

    @Resource
    private ILogService logService;

    @GetMapping("list")
    @RequiresPermissions("log:view")
    public ResultUtil logList(SystemLog log, QueryRequest request) {
        IPage<SystemLog> page1 = this.logService.findLogs(log, request);
        ResultUtil<Object> success = ResultUtil.success(Constant.SELECT_SUCCESS);
        success.setCount(page1.getTotal());
        success.setData(page1.getRecords());
        return success;
    }

    @GetMapping("delete/{ids}")
    @RequiresPermissions("log:delete")
    @ControllerEndpoint(exceptionMessage = "删除日志失败")
    public FebsResponse deleteLogss(@NotBlank(message = "{required}") @PathVariable String ids) {
        String[] logIds = ids.split(StringPool.COMMA);
        this.logService.deleteLogs(logIds);
        return new FebsResponse().success();
    }

//    @GetMapping("excel")
//    @RequiresPermissions("log:export")
//    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
//    public void export(QueryRequest request, SystemLog lg, HttpServletResponse response) {
//        List<SystemLog> logs = this.logService.findLogs(lg, request).getRecords();
//        ExcelKit.$Export(SystemLog.class, response).downXlsx(logs, false);
//    }
}
