package com.macw.wwdoc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("api_test")
public class Test implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 1111
     */
    @TableId(value = "aaa_id", type = IdType.AUTO)
    private Integer aaaId;

    /**
     * ss
     */
    private String name;

    private Integer age;

    private String xxx;


}
